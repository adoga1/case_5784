-- Filename: complex_query.sql

-- This query retrieves detailed information about customers and their orders,
-- including the total amount spent, average order value, and the number of orders.
-- It also filters for customers who have spent more than a certain threshold and
-- orders placed within a specific date range.

-- Drop the view if it already exists
DROP VIEW IF EXISTS customer_order_summary;

-- Create a view to summarize customer order details
CREATE VIEW customer_order_summary AS
SELECT
    c.customer_id,
    c.first_name,
    c.last_name,
    c.email,
    COUNT(o.order_id) AS total_orders,
    SUM(od.quantity * p.price) AS total_spent,
    AVG(od.quantity * p.price) AS average_order_value
FROM
    customers c
    INNER JOIN orders o ON c.customer_id = o.customer_id
    INNER JOIN order_details od ON o.order_id = od.order_id
    INNER JOIN products p ON od.product_id = p.product_id
WHERE
    o.order_date BETWEEN '2023-01-01' AND '2023-12-31'
GROUP BY
    c.customer_id, c.first_name, c.last_name, c.email
HAVING
    SUM(od.quantity * p.price) > 1000;

-- Query the view to get detailed information about high-value customers
SELECT
    customer_id,
    first_name,
    last_name,
    email,
    total_orders,
    total_spent,
    average_order_value
FROM
    customer_order_summary
ORDER BY
    total_spent DESC;

-- Complex query to retrieve product details and their sales performance
SELECT
    p.product_id,
    p.product_name,
    p.category,
    p.price,
    COALESCE(SUM(od.quantity), 0) AS total_quantity_sold,
    COALESCE(SUM(od.quantity * p.price), 0) AS total_revenue,
    (SELECT AVG(r.rating)
     FROM reviews r
     WHERE r.product_id = p.product_id) AS average_rating
FROM
    products p
    LEFT JOIN order_details od ON p.product_id = od.product_id
GROUP BY
    p.product_id, p.product_name, p.category, p.price
ORDER BY
    total_revenue DESC;

-- Query to find the top 5 customers based on total spending
SELECT
    c.customer_id,
    c.first_name,
    c.last_name,
    SUM(od.quantity * p.price) AS total_spent
FROM
    customers c
    INNER JOIN orders o ON c.customer_id = o.customer_id
    INNER JOIN order_details od ON o.order_id = od.order_id
    INNER JOIN products p ON od.product_id = p.product_id
GROUP BY
    c.customer_id, c.first_name, c.last_name
ORDER BY
    total_spent DESC
LIMIT 5;
